<?php

namespace laylatichy\nano\modules\aws\modules;

use Aws\CloudFront\CloudFrontClient;

class Cloudfront {
    public function __construct(
        public readonly CloudFrontClient $client,
    ) {}

    public function sign(
        string $base_url,
        string $file_path,
        string $private_key,
        string $key_pair_id,
        int $expires = 1800,
    ): string {
        $policy = json_encode([
            'Statement' => [
                [
                    'Resource'  => "{$base_url}/*",
                    'Condition' => [
                        'DateLessThan' => [
                            'AWS:EpochTime' => time() + $expires,
                        ],
                    ],
                ],
            ],
        ]);

        return $this->client->getSignedUrl([
            'url'         => "{$base_url}/{$file_path}",
            'key_pair_id' => $key_pair_id,
            'private_key' => $private_key,
            'policy'      => $policy,
        ]);
    }
}
