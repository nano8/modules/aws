<?php

namespace laylatichy\nano\modules\aws\modules;

use Aws\Result;
use Aws\Sqs\SqsClient;

class Sqs {
    public function __construct(
        public readonly string $name,
        public readonly string $url,
        public readonly SqsClient $client,
    ) {}

    public function send(array $payload, array $args = []): Result {
        return $this->client->sendMessage([
            'QueueUrl'       => $this->url,
            'MessageGroupId' => $this->name,
            'MessageBody'    => json_encode($payload),
            ...$args,
        ]);
    }

    public function receive(int $messages = 10, int $wait = 0, array $args = []): Result {
        return $this->client->receiveMessage([
            'QueueUrl'            => $this->url,
            'MaxNumberOfMessages' => $messages,
            'WaitTimeSeconds'     => $wait,
            ...$args,
        ]);
    }

    public function delete(string $handle, array $args = []): Result {
        return $this->client->deleteMessage([
            'QueueUrl'      => $this->url,
            'ReceiptHandle' => $handle,
            ...$args,
        ]);
    }
}
