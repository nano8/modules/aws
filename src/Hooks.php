<?php

use laylatichy\nano\modules\aws\AwsModule;
use laylatichy\nano\modules\aws\modules\Cloudfront;
use laylatichy\nano\modules\aws\modules\Sqs;

if (!function_exists('useAwsSqs')) {
    function useAwsSqs(string $name, string $url): Sqs {
        return useNanoModule(AwsModule::class)
            ->aws
            ->sqs($name, $url);
    }
}

if (!function_exists('useAwsCloudfront')) {
    function useAwsCloudfront(): Cloudfront {
        return useNanoModule(AwsModule::class)
            ->aws
            ->cloudfront();
    }
}