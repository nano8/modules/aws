<?php

namespace laylatichy\nano\modules\aws;

use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

class AwsModule implements NanoModule {
    public Aws $aws;

    public function __construct(
        private readonly string $profile,
        private readonly string $region,
        private readonly string $version,
    ) {
        // nothing to do here
    }

    public function register(Nano $nano): void {
        if (isset($this->aws)) {
            useNanoException('aws module already registered');
        }

        $this->aws = new Aws(
            new Credentials(
                $this->profile,
                $this->region,
                $this->version
            )
        );
    }
}
