<?php

namespace laylatichy\nano\modules\aws;

use Aws\CloudFront\CloudFrontClient;
use Aws\Sqs\SqsClient;
use laylatichy\nano\modules\aws\modules\Cloudfront;
use laylatichy\nano\modules\aws\modules\Sqs;

class Aws {
    public function __construct(
        private readonly Credentials $credentials,
    ) {}

    public function sqs(string $name, string $url): Sqs {
        return new Sqs(
            $name,
            $url,
            new SqsClient([
                'profile' => $this->credentials->profile,
                'region'  => $this->credentials->region,
                'version' => $this->credentials->version,
            ])
        );
    }

    public function cloudfront(): Cloudfront {
        return new Cloudfront(
            new CloudFrontClient([
                'profile' => $this->credentials->profile,
                'region'  => 'us-east-1',
                'version' => $this->credentials->version,
            ])
        );
    }
}
