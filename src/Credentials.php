<?php

namespace laylatichy\nano\modules\aws;

class Credentials {
    public function __construct(
        public readonly string $profile,
        public readonly string $region,
        public readonly string $version,
    ) {}
}
