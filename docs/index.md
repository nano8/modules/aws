---
layout: home

hero:
    name:    nano/modules/aws
    tagline: aws module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/aws is a module for aws
    -   title:   aws
        details: |
                 nano/modules/aws provides a simple way to use aws services in your nano application
---
