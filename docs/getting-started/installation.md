---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-aws
```

## registering the module

```php
use laylatichy\nano\modules\aws\AwsModule;

useNano()->withModule(new AwsModule('default', 'us-east-1', 'latest'));
```

