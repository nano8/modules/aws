---
layout: doc
---

<script setup>
const args = {
    defineAwsSqs: [
        { name: 'name', type: 'string' },
        { name: 'url', type: 'string' },
    ],
    useAwsSqs:[],
    send: [
        { name: 'payload', type: 'array' },
    ],
    receive: [
        { name: 'messages', type: '?int' },
        { name: 'wait', type: '?int' },
    ],
    delete: [
        { name: 'handle', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\aws\modules\Sqs

## <Types fn="defineAwsSqs" :args="args.defineAwsSqs" r="void" /> {#defineAwsSqs}

needs to be called after initializing aws module and before starting nano

```php
defineAwsSqs('default', 'https://sqs.us-east-1.amazonaws.com/123456789012/MyQueue');
```

## <Types fn="useAwsSqs" :args="args.useAwsSqs" r="Sqs" /> {#useAwsSqs}

```php
useAwsSqs();
```

## <Types fn="send" :args="args.send" r="Result" /> {#send}

```php
useAwsSqs()->send(['foo' => 'bar']);
```

## <Types fn="receive" :args="args.receive" r="Result" /> {#receive}

```php
useAwsSqs()->receive();
```

## <Types fn="delete" :args="args.delete" r="Result" /> {#delete}

```php
useAwsSqs()->delete('handle');
```

